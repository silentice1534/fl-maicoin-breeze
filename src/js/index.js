// getElement

const getEle = (selector) => {
  return document.querySelector(selector);
};

// menu

const registerEvent = () => {
  const menu = getEle('#menu');
  const hbg = getEle('#hbg');
  menu.addEventListener('click', (e) => {
    const targetSectionId = e.srcElement.getAttribute('data-target');
    hbg.checked = false;

    getEle(`#${targetSectionId}`).scrollIntoView({
      behavior: 'smooth',
    });
  });
};

registerEvent();

// 倒數

const renderCountdownTime = (d, h, m) => {
  const dayNum = getEle('#countdown-day');
  const hourNum = getEle('#countdown-hour');
  const minNum = getEle('#countdown-min');

  if (d < 0) {
    dayNum.textContent = '0';
  } else {
    dayNum.textContent = d;
  }

  if (h < 0) {
    hourNum.textContent = '0';
  } else {
    hourNum.textContent = h;
  }

  if (m < 0) {
    minNum.textContent = '0';
  } else {
    minNum.textContent = m;
  }
};

const renderCountDownUnit = (d, h, m) => {
  const dayUnit = getEle('#countdown-unit-day');
  const hourUnit = getEle('#countdown-unit-hour');
  const minUnit = getEle('#countdown-unit-min');

  dayUnit.textContent = 'day';
  if (d > 1) {
    dayUnit.textContent += 's';
  }

  hourUnit.textContent = 'hour';
  if (h > 1) {
    hourUnit.textContent += 's';
  }

  minUnit.textContent = 'min';
  if (m > 1) {
    minUnit.textContent += 's';
  }
};

let timer = null;
const startTimer = () => {
  timer = setTimeout(() => {
    clearTimeout(timer);
    remaingTime();
  }, 1000);
};

const remaingTime = () => {
  const targetDate = '2022-04-30'; // 指向目標日期的 'YYYY-MM-DD 00:00:00'

  const [y, m, d] = targetDate.split('-').map((ele) => Number(ele));

  const targetDateTimeStamp = new Date(y, m - 1, d).getTime();
  const currTimeTimeStamp = Date.now();
  const remainingTime = targetDateTimeStamp - currTimeTimeStamp;

  // 剩餘天
  const day = remainingTime / 1000 / 60 / 60;
  const remaingDays = Math.trunc(day / 24);
  const remaingHours = Math.trunc(day % 24);
  const remaingMinsInPersentage = (day % 24) - Math.trunc(day % 24);
  const remaingMins = Math.ceil(60 * remaingMinsInPersentage);

  renderCountdownTime(remaingDays, remaingHours, remaingMins);
  renderCountDownUnit(remaingDays, remaingHours, remaingMins);
  startTimer();
};

remaingTime();

// faq 動畫

const accordionAnimate = () => {
  const recordEleHeight = {};

  const faqList = ['faq1', 'faq2', 'faq3', 'faq4', 'faq5', 'faq6', 'faq7', 'faq8'];

  faqList.forEach((ele) => {
    const ques = getEle(`#${ele}`);
    const ans = getEle(`#${ele}-ans`);

    recordEleHeight[ele] = ans.offsetHeight + 'px';
    ans.style.height = recordEleHeight[ele];
    ans.style.height = '0px';

    const toggle = () => {
      let activted = false;

      return () => {
        activted = !activted;

        const icon = getEle(`#${ele} .faq-item__ques__add__icon`);
        // const ansContent = getEle(`#${ele} .faq-item__ans`);
        // console.log('ansContent', ansContent);
        if (activted) {
          console.log('ans', ans);
          ans.style.height = recordEleHeight[ele];
          icon.style.transform = `rotate(45deg)`;
          ans.style.borderTop = '1px solid #ffffff';
        } else {
          ans.style.height = '0px';
          ans.style.borderTop = '0px';
          icon.style.transform = `rotate(0deg)`;
        }
      };
    };

    ques.addEventListener('click', toggle());
  });
};

accordionAnimate();
